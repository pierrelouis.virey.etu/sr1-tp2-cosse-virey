/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
package fil.sr1.user;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
public class UserManagerTest {

	private UserManager userManager;
	private User user1;
	
	@Before
	public void init() {
		userManager = new UserManager();
		user1 = new User("Théophile","azerty");
		userManager.addUser(user1);
	}
	
	@Test
	public void addUserIsAddingUserTest() {
		User pl = new User("PL","hAck3r");
		userManager.addUser(pl);
		assertEquals(userManager.getUsers().get("PL"),pl);
	}
	
	@Test
	public void isUserIsWorkingTest() {
		assertTrue(userManager.isUser(user1.getUserName()));
		assertFalse(userManager.isUser("Mr Propre"));
	}
	
	@Test
	public void removeUserIsRemovingUserTest() {
		userManager.removeUser(user1);
		assertFalse(userManager.isUser(user1.getUserName()));
	}
	
	@Test
	public void getPasswordReturnsGoodPasswordTest() {
		assertEquals(userManager.getPassword(user1.getUserName()),user1.getPassword());
	}
}
