/**
 * @author Pierre-Louis Virey
 * 14 févr. 2021
 */
package fil.sr1;

import static org.junit.Assert.assertTrue;

import java.net.Socket;

import org.junit.Test;

import fil.sr1.exception.FTPClientThreadCreationException;
import fil.sr1.exception.WrongArgumentsException;

/**
 * @author Pierre-Louis Virey
 * 14 févr. 2021
 */
public class FTPClientThreadTest {
	
	@Test(expected=FTPClientThreadCreationException.class)
    public void ExceptionIsRaisedWhenNotCreatedProperlyTest() {
    	FTPClientThread ct = new FTPClientThread(new Socket(), null, null);
    }
	
}
