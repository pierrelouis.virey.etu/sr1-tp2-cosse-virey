/**
 * @author Pierre-Louis Virey
 * 7 févr. 2021
 */
package fil.sr1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.*;

import fil.sr1.user.UserManager;

/**
 * @author Pierre-Louis Virey
 * 7 févr. 2021
 */
public class FTPServer {
	public static final String DEFAULT_REPOSITORY = "./ftpdir";
	
	private int port;
	private String root;
	private UserManager userManager;
	private ServerSocket servSkt;
	
	public int getPort() {
		return port;
	}

	public String getRoot() {
		return root;
	}

	/**
	 * Construit le serveur FTP avec le repertoire par défaut
	 * @param port le port à utiliser pour se connecter
	 * @param userManager le manager d'utilisateur
	 */
	public FTPServer(int port, UserManager userManager) {
		this.port = port;
		this.userManager = userManager;
		this.root = DEFAULT_REPOSITORY;
	}
	
	/**
	 * Construis le serveur FTP
	 * @param port le port à utiliser pour se connecter
	 * @param userManager le manager d'utilisateur
	 * @param root le repertoire racine du serveur
	 */
	public FTPServer(int port, UserManager userManager, String root) {
		this.port = port;
		this.userManager = userManager;
		this.root = root;
	}
	
	/**
	 * Start the FTP server.
	 * @throws IOException 
	 */
	public void start() throws IOException {
		this.servSkt = new ServerSocket(port);
		Socket client = null;
		while(true) {
			System.out.println("En attente de connection.");
			try {
				FTPClientThread ct = new FTPClientThread(servSkt.accept(), root, userManager);
				ct.start();				
			} catch (SocketException e) {
				throw new RuntimeException("Impossibilité d'accepter un socket, interruption du serveur FTP.",e);
			}
		}
	}

	public void close() throws IOException {
		this.servSkt.close();
	}
}
