/**
 * @author Pierre-Louis Virey
 * 13 févr. 2021
 */
package fil.sr1;

/**
 * @author Pierre-Louis Virey
 * 13 févr. 2021
 * 
 * Contient tous les code FTP utile au fonctionnement de l'application.
 */
public final class FTPCode {
	public static final String C125 = "125 Canal de donnees deja ouvert; debut de transfert.";
	public static final String C150 = "150 Statut de fichier vérifie; ouverture de canal de donnees en cours.";
	
	public static final String C200 = "200 Commande conclue.";
	public static final String C220 = "220 Service disponible pour nouvel utilisateur.";
	public static final String C226 = "226 Fermeture du canal de donnees. Service termine.";
	public static final String C227 = "227 Entree en mode passif";
	public static final String C230 = "230 Session ouverte.";
	public static final String C250 = "250 Service fichier termine.";
	public static final String C257 = "257 ";	//CWD ok.
	
	public static final String C331 = "331 Nom d'utilisateur reçu, mot de passe demande";
	public static final String C350 = "350 Service fichier en attente d'information.";
	
	public static final String C425 = "425 Erreur d'ouverture du canal de donnees";
	public static final String C451 = "451 Service interrompu. Erreur locale de traitement.";
	
	public static final String C501 = "501 Erreur de syntaxe dans le parametres ou arguments.";
	public static final String C502 = "502 Commande non implementee.";
	public static final String C503 = "503 Mauvaise sequence de commandes.";
	public static final String C530 = "530 Session non ouverte.";
	public static final String C550 = "550 Service fichier non traite. Fichier non accessible";
}
