/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
package fil.sr1.exception;

/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
public class WrongArgumentsException extends Exception{
	public WrongArgumentsException() {
		super("Incorrect syntax please refer to the following pattern :\n"
				+"java -jar ServFTP.jar server_port [directory]");
	}
}
