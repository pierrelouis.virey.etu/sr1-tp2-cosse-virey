/**
 * @author Théophile Cosse
 * 7 Mars 2021
 */
package fil.sr1.exception;

public class FTPClientThreadCreationException extends RuntimeException{
	public FTPClientThreadCreationException(String msg) {
		super("Echec lors de la création des Reader / Writer. \n" + msg);
	}
}