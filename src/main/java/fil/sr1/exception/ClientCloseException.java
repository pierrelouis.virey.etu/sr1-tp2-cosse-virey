/**
 * @author Pierre-Louis Virey
 * 6 mars 2021
 */
package fil.sr1.exception;

/**
 * @author Pierre-Louis Virey
 * 6 mars 2021
 */
public class ClientCloseException extends RuntimeException {
	public ClientCloseException(String msg) {
		super("Impossible de fermer la connection.\n"+msg);
	}
}
