/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
package fil.sr1.user;

import java.util.HashMap;

/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
public class UserManager {
	
	private HashMap<String,User> users;		//La map contenant les utilisateur du service.
	
	public UserManager() {
		this.users = new HashMap<String, User>();
	}
	
	/**
	 * @return the users
	 */
	public HashMap<String, User> getUsers() {
		return users;
	}
	
	/**
	 * Ajoute un user à la map des utilisateurs.
	 */
	public void addUser(User user) {
		users.put(user.getUserName(),user);
	}
	
	/**
	 * Supprime un user à la map des utilisateurs.
	 */
	public void removeUser(User user) {
		users.remove(user.getUserName(),user);
	}
	
	/**
	 * Verifie que l'utilisateur existe dans la liste
	 * @param user le nom d'uitlisateur
	 * @return true si oui sinon false
	 */
	public boolean isUser(String user) {
		return users.containsKey(user);
	}
	
	/**
	 * Renvoie le mot de passe de l'utilisateur
	 * @param userName l'userName de l'user conercnée.
	 */
	public String getPassword(String userName) {
		return users.get(userName).getPassword();
	}
	
}
