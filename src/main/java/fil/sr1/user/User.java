/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
package fil.sr1.user;

/**
 * @author Pierre-Louis Virey
 * 26 févr. 2021
 */
public class User {
	private String userName;
	private String password;
	
	/**
	 * @param userName
	 * @param password
	 */
	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}
	
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
}
