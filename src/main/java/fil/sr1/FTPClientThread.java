/**
 * @author Pierre-Louis Virey
 * 7 févr. 2021
 */
package fil.sr1;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

import fil.sr1.exception.ClientCloseException;
import fil.sr1.exception.FTPClientThreadCreationException;
import fil.sr1.user.UserManager;

/**
 * Cette classe permet de gérer les commandes envoyée au serveur par un utilisateur.
 * 
 * @author Pierre-Louis Virey
 * 7 févr. 2021
 */
public class FTPClientThread extends Thread {
	
	private enum DATA_TYPE {
		A,E,I,L
	}
	
	public static final boolean TALK = true;			//Display the server messages if true

	public static final String IP = "127.0.0.1";

	private String homeDirectory;
	private String username;
	private UserManager userManager;
	
	private Socket skt;
	private BufferedReader in;
	private PrintWriter out;
	private String curDirectory = "/";
	private DATA_TYPE type;
	private Socket sktData;
	private BufferedReader inData;
	private PrintWriter outData;
	private boolean isAuthentified = false;
	private boolean isUsingPASV = false;
	private ServerSocket servSkt;
	
	/**
	 * Construis le thread client executant les commandes.
	 * @param skt le socket du client.
	 * @param homeDirectory le repertoire du serveur.
	 * @param userManager le manager d'utilisateur.
	 */
	public FTPClientThread(Socket skt, String homeDirectory, UserManager userManager) {
		this.homeDirectory = homeDirectory;
		this.userManager = userManager;
		this.skt = skt;
		try {
			this.in = new BufferedReader(new InputStreamReader(skt.getInputStream()));
			this.out = new PrintWriter(skt.getOutputStream(), true);
		} catch (IOException e) {
			throw new FTPClientThreadCreationException(e.getMessage());
		}
	}
	
	/**
	 * Permet de terminer la connection.
	 */
	public void closeConnexion() {
		try {
			this.skt.close();
			this.in.close();
			this.out.close();
		} catch (IOException e) {
			throw new ClientCloseException(e.getMessage());
		}
	}
	
	/**
	 * Lis les commandes du client et les executent.
	 */
	public void run() {
		System.out.println("Un client viens de se connecter.");
		
		try {
			out.println(FTPCode.C220);					// Message d'accueil
			String reponse;

			// On attend les commandes client.
			while(true) {
				reponse = in.readLine();
				if (reponse == null) {					// Interuption de la connexion au serveur.
					if (this.username != null)
						System.out.println("Connection de "+ this.username +" au serveur FTP interrompue.");
					break;
				} else {
					if(TALK)
						System.out.println(reponse);
					parseResponse(reponse);				// Parsage de le réponse pour pouvoir executer la commande.
				}
			}
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		} finally {
			closeConnexion();
		}
	}
	
	/**
	 * Parse la réponse du client afin d'executer les actions demandées.
	 * @param reponse envoyé par le client.
	 */
	private void parseResponse(String reponse) {
		String command;
		String arg;
		
		if(reponse.contains(" ")) {		//Vrai si on as un argument.
			String[] splitedReponse = reponse.split(" ");
			command = splitedReponse[0];
			arg = splitedReponse[1];
		} else {
			command = reponse;
			arg = null;
		}
		
		if (!this.isAuthentified) {
			authentify(command, arg);
		} else {
			chooseAction(command, arg);
		}
	}
	
	/**
	 * Choisis les fonctions à effectuer pour authentifier un utilisateur.
	 * @param command la commande à executer
	 * @param arg les argumetns de la commande
	 */
	private void authentify(String command, String arg) {
		switch (command) {
		case "USER":
			do_USER(arg);
			break;
		case "PASS":
			do_PASS(arg);
			break;
		default:
			out.println(FTPCode.C503);
		}
	}

	/**
	 * Choisis les fonctions à effectuer afin de répondre à la demande de l'utilisateur.
	 * @param command la commande à executer
	 * @param arg les argumetns de la commande
	 */
	private void chooseAction(String command, String arg) {
		switch (command) {
		case "PWD":
			out.println(FTPCode.C257 + curDirectory);
			break;
		case "TYPE":
			out.println(do_TYPE(arg));
			break;
		case "PASV":
			do_PASV();
			break;
		case "PORT":
			out.println(do_PORT(arg));
			break;
		case "LIST":
			do_LIST(arg);
			break;
		case "CWD":
			do_CWD(arg);
			break;
		case "MKD":
			do_MKD(arg);
			break;
		case "CDUP":
			do_CDUP();
			break;
		case "RETR":
			do_RETR(arg);
			break;
		case "STOR":
			do_STOR(arg);
			break;
		case "RNFR":
			do_RNFR_RNTO(arg);
			break;
		case "RNTO":
			out.println(FTPCode.C503); // Mauvaise séquence on attend RNFR avant.
			break;
		case "RMD":
			do_RM(arg);
			break;
		case "DELE":
			do_RM(arg);
			break;
		default:
			out.println(FTPCode.C502);
		}
	}
	
	/**
	 * Identification avec un username.
	 */
	private void do_USER(String username) {
		this.username = username;	//Identifiant du client
		out.println(FTPCode.C331);	//Dans tous les cas on "accepte la connexion" même si l'utilisateur est incconu.
	}
	
	/**
	 * On attend le mot de passe pour finaliser la connexion.
	 */
	private void do_PASS(String password) {
		//Seulement ici on vérifie que l'user existe et le mdp correspondent.
		if(userManager.isUser(this.username) && userManager.getPassword(this.username).equals(password)) {
			out.println(acceptClient());		//Le client est reconnu on accepte sa connexion.
		} else {
			out.println(FTPCode.C530);			//Le client est inconnu fermeture de la connexion.
		}
	}
	
	/**
	 * Crée un repertoire pour l'user client.
	 * @return le code de validation de la commande
	 */
	private String acceptClient(){
		this.isAuthentified = true;		//La connexion est désormé authentifiée
		File dir = new File(this.homeDirectory+'/'+this.username);
		this.homeDirectory += '/'+this.username;
		if (!dir.exists() || !dir.isDirectory()){	//On crée le repertoire de l'user si besoin.
			if(dir.mkdir()){
				if(TALK)
					System.out.println("Repertoire client crée");
				return FTPCode.C230;
			} else {
				if(TALK)
					System.out.println("Echec de la création du repertoire client.");
				return FTPCode.C451;
			}
		}
		return FTPCode.C230;
	}
	
	/**
	 * Creer les sockets permetant la communication passive et envoie au client les informations nécessaire pour sa connection.
	 */
	private void do_PASV() {
		// La convention est d'établire le port de donnée sur (port de controle - 1)
		int port = this.skt.getPort()-1;
		String formatedIP = IP.replace('.', ',');
		int port1 = port / 256;
		int port2 = port % 256;

		// Boolean permettant de savoir si fermer le ServerSocket et necessaire par la suite.
		this.isUsingPASV = true;

		String pasvInfos = FTPCode.C227 + " (" + formatedIP + "," + String.valueOf(port1) + "," + String.valueOf(port2) + ")";
		
		out.println(pasvInfos);		//Envoie des informations au client.

		try {
			this.servSkt = new ServerSocket(port);
			this.sktData = servSkt.accept();			
			//Création du socket permettant la connection.
			this.inData = new BufferedReader(new InputStreamReader(sktData.getInputStream()));
			this.outData = new PrintWriter(sktData.getOutputStream(), true);
		} catch (IOException e) {
			out.println(FTPCode.C425);
		}
	}
	
	/**
	 * Change le format du type d'échange de fichier
	 */
	private String do_TYPE(String str_type) {
		switch (str_type) {
		case "I":
			this.type = DATA_TYPE.I;
			break;
		case "A":
			this.type = DATA_TYPE.A;
			break;
		default:
			return FTPCode.C502;
		}
		return FTPCode.C200;
	}
	
	/**
	 * Create the socket for the communication
	 * @param socketAdr
	 */
	private String do_PORT(String socketAdr) {
		
		String[] repSplited = socketAdr.split(",");
		String ipAdress = repSplited[0] + "." + repSplited[1] + "." + repSplited[2] + "." + repSplited[3];
		// Calcul du port à utiliser pour la connection.
		int port = Integer.parseInt(repSplited[4]) * 256 + Integer.parseInt(repSplited[5]);
		
		System.out.println(ipAdress);
		System.out.println(port);
		
		try {
			this.sktData = new Socket(ipAdress, port);
			//Création du socket permettant la connection.
			this.inData = new BufferedReader(new InputStreamReader(sktData.getInputStream()));
			this.outData = new PrintWriter(sktData.getOutputStream(), true);
		} catch (IOException e) {
			return FTPCode.C501;
		}
		
		return FTPCode.C200;
	}
	
	/**
	 * Close the data conection after a transfer.
	 */
	private void closeDataConnection() {
		try {
			this.sktData.close();
			this.outData.close();
			this.inData.close();
			if (this.isUsingPASV) {
				this.servSkt.close();
			}
			out.println(FTPCode.C226);
		} catch (IOException e) {
			out.println(FTPCode.C451);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Execute la commande LIST sur le repertoire courant.
	 */
	private void do_LIST(String directory) {
		out.println(FTPCode.C125);
		
		String[] res = listDirectory(directory);
		for (int i = 2; i<res.length;i++) {
			this.outData.println(res[i]);
		}
		
		closeDataConnection();
	}
	
	/**
	 * Execute the ls command on the direcotry
	 * @param directory, if null the root folder will be lsed
	 */
	private String[] listDirectory(String directory) {
		//En premier on recupère le dossier à liste, ici on prend un dossier test à la racine du PC
		String path;
		if(directory!=null) {
			path = this.homeDirectory + "/" + directory;
		} else {
			path = this.homeDirectory + "/" + this.curDirectory;
		}
		
		File file = new File(path);
		
		if(file.isDirectory()) {	//Si on liste bien un dossier un dossier
			String[] fakeArgs = {"ls",path,"-al"};		//Les arguments de la commende.
			 
	        ProcessBuilder processBuilder = new ProcessBuilder(fakeArgs);	//Creation du process builder qui execute la commande.
	        processBuilder.redirectErrorStream(true);
	 
	        Process process;
			try {
				process = processBuilder.start();
				String[] res = createArray(process.getInputStream());
		        process.waitFor();
		        //System.out.println("Exit Status : " + process.exitValue());
		        return res;
			} catch (IOException | InterruptedException e) {
				out.println(FTPCode.C451);
			}
	        return null;
		} else {	//un fichier
			String[] fName = {file.getName()};
			return fName;
		}
	}
	
	/**
	 * Creation d'un tableau qui contient le resultat de la commande ls sous le format suivant : -rw-r--r-- 1 pl pl  369 README.md
	 * @param in the InputStream of the execution.
	 * @return the list wich contains the array
	 * @throws IOException
	 */
	private String[] createArray(InputStream in) throws IOException {
        StringBuilder sb = new StringBuilder();
		while (true) {
            int c = in.read();
            if (c == -1) {
                break;
            }
            sb.append((char) c);
        }
		//On enlève les char qui font bugger le transfert.
		String res = sb.toString();
		String[] res2 = res.split("\\r?\\n");
 		
		//On sauvegarde les informations sous le format reconnu par la norme.
		for(int i=2; i<res2.length; i++) {
			String[] tmp = res2[i].split("\\s+");
			res2[i] = tmp[0] + " " + tmp[1] + " " + tmp[2] + " " + tmp[3] + " " + tmp[4] + " " + tmp[8];
		}
		
		return res2;
    }
	
	/**
	 * Permet de changer le repertoire de travail courant. 
	 */
	private void do_CWD(String directory) {
		String dirPath;
		
		if(directory.contains("..")) {				// Empeche l'utilisateur de remonter dans les fichiers du serveur.
			out.println(FTPCode.C501 + " Le serveur ne supporte pas les CWD avec ..");
		}
		
		if(directory.startsWith("/")) {				// cd est absolu par rapport au serveur.
			dirPath = homeDirectory + directory;
			File f = new File(dirPath);
			
			if(f.exists() && f.isDirectory()) {
				this.curDirectory = directory;		// Mise à jour du current directory.
				out.println(FTPCode.C250);
			} else {
				out.println(FTPCode.C550);			// Le fichier n'est pas accessible.
			}
			
		} else {									// cd est relatif au repertoire courant.
			dirPath = homeDirectory + this.curDirectory + "/" + directory;
			File f = new File(dirPath);
			
			if(f.exists() && f.isDirectory()) {
				this.curDirectory =  this.curDirectory + "/" + directory;
				out.println(FTPCode.C250);
			} else {
				out.println(FTPCode.C550);			// Le fichier n'est pas accessible.
			}
		}
	}
	
	/**
	 * Create a directory.
	 * @param directory the name of the file
	 */
	private void do_MKD(String directory) {
		if(directory==null) {
			directory = "Untiteld";
		}
		
		String dirPath = homeDirectory + this.curDirectory + "/" + directory;
		File f = new File(dirPath);
		
		if(f.mkdir()) {
			out.println(FTPCode.C257);
		} else {
			out.println(FTPCode.C501);
		}
		
	}
	
	/**
	 * Remonte au repertoire parent le dossier courant du serveur.
	 */
	private void do_CDUP() {
		if(!curDirectory.equals("/")) {
			int index = 0;
			for(int i=curDirectory.length()-1; i>0; i--) {	//On cherche l'index du dernier /
				if(curDirectory.charAt(i) == '/') {
					index = i;
					break;
				}
			}
			if(index==0) {
				curDirectory = "/";
			} else {
				curDirectory = curDirectory.substring(0,index);	//On troncate la chaine pour remonter à repertoire parent.
			}	
		}
		out.println(FTPCode.C250);
	}
	
	/**
	 * Télécharge le fichier filename du serveur vers le client.
	 */
	private void do_RETR(String fileName) {
		String dirPath = homeDirectory + this.curDirectory + "/" + fileName;
		
		if(this.type == DATA_TYPE.A) {			//Transfere en mode ASCII
			out.println(FTPCode.C125);
			
			File f = new File(dirPath);
			
			Scanner sc;
			try {
				sc = new Scanner(f);
				while(sc.hasNextLine()) {
					outData.println(sc.nextLine()); 
			  	}
			} catch (FileNotFoundException e1) {
				out.println(FTPCode.C550);
				e1.printStackTrace();
				return;		//Exit la func si on as pas put ouvrir le fichier.
			} 
			
			sc.close();
			
			closeDataConnection();	//Fermeture de la connection.
			
		} else if(this.type == DATA_TYPE.I) {	//Transfere binaire ex: image
			out.println(FTPCode.C125);
	        try (
	            InputStream inputStream = new FileInputStream(dirPath);		
	        	BufferedOutputStream realOut = new BufferedOutputStream(sktData.getOutputStream()); //Pas la peine de fermer avec le try().
	        ) {
	            byte[] buffer = new byte[4096];		//On lis 4KB du fichier binaire à chaque fois.
	            int sizeRead = 0;					//La taille lu
	            
	            while ((sizeRead = inputStream.read(buffer)) != -1) {
	            	realOut.write(buffer,0,sizeRead);	//On fais bien attention de ne pas envoyer les octets en trop à la fin.
	            }
	        } catch (IOException e) {
	            out.println(FTPCode.C451);
	        	System.err.println("Un fichier n'a pas pu être téléchargé.");
	        }
	        
	        closeDataConnection();	//Fermeture de la connection.
		}
		else {
			out.println(FTPCode.C550);	//Pour l'instant on n'as pas encore les autres modes.
		}
	}
	
	/**
	 * Envoie une copie du fichier spécifié par filePath au client.
	 */
	private void do_STOR(String fileName) {
		String dirPath = homeDirectory + this.curDirectory + "/" + fileName;

		// Creation du fichier. Que faire si il existe deja ?
		File f = new File(dirPath);
		// Si le fichier entièrement spécifié existe sur le serveur avant la transmission, alors son contenu sera remplacé par le contenu transmis.
		if(f.exists()) {
			try {
				new PrintWriter(dirPath).close();
			} catch (IOException e) {
				out.println(FTPCode.C550);
				System.err.println("Un fichier n'a pas pu être envoyé.");
				return;
			}
		}
		
		if (this.type == DATA_TYPE.A) { // Transfere en mode ASCII
			out.println(FTPCode.C125);

			try (FileWriter fw = new FileWriter(dirPath, true);
				BufferedWriter bw = new BufferedWriter(fw);
				PrintWriter fOut = new PrintWriter(bw);) {
				String line = "";
				while ((line = inData.readLine()) != null) {
					fOut.println(line);
				}
			} catch (IOException e) {
				out.println(FTPCode.C550);
				System.err.println("Un fichier n'a pas pu être envoyé.");
				return;
			}
			closeDataConnection(); // Fermeture de la connection.
		} else if (this.type == DATA_TYPE.I) { // Transfere binaire ex: image
			out.println(FTPCode.C125); 
			try (
				FileOutputStream output = new FileOutputStream(dirPath, true);
		        BufferedInputStream realIn = new BufferedInputStream(sktData.getInputStream());)
				{
					byte[] buffer = new byte[4096]; // On lis 4KB du fichier binaire à chaque fois.
					int sizeRead = 0; // La taille lu

					while ((sizeRead = realIn.read(buffer)) != -1) {
						output.write(buffer, 0, sizeRead); // On fais bien attention de ne pas envoyer les octets en trop à la fin.
					}
				} catch (IOException e) {
					out.println(FTPCode.C451);
					System.err.println("Un fichier n'a pas pu être envoyé.");
				}
			 closeDataConnection(); // Fermeture de la connection.
			} else {
			out.println(FTPCode.C550); // Pour l'instant on n'as pas encore les autres modes.
		}
	}
	
	/**
	 * Permet de renommer un fichier.
	 */
	private void do_RNFR_RNTO(String oldName) {
		out.println(FTPCode.C350);
		//On attend le nouveau nom
		String reponse = "";
		try {
			reponse = in.readLine();
		} catch (IOException e) {
			out.println(FTPCode.C451);
			System.err.println("Un fichier n'a pas pu être renommé.");
		}
		
		if(TALK) { System.out.println(reponse);}
		
		if(reponse.startsWith("RNTO ")) {
			String newName = reponse.split(" ")[1];
			
			// L'ancien fichier
			File file = new File(homeDirectory +this.curDirectory+"/"+oldName);
			// Un fichier qui porterai le même nom
			File file2 = new File(homeDirectory +this.curDirectory+"/"+newName);

			if (file2.exists())
			   out.println(FTPCode.C550);
			else {
				file.renameTo(new File(homeDirectory +this.curDirectory+"/"+newName));
				out.println(FTPCode.C250);
			}
		} else {	//On attendais un RNTO mais on a eu autre chose, mauvaise séquence de commande.
			out.println(FTPCode.C503);
		}

	}
	
	/**
	 * Permet de supprimer un dossier
	 * @param file le fichier à supprimer
	 */
	private void do_RM(String fileName) {
		String dirPath = homeDirectory + this.curDirectory + "/" + fileName;
		
		File f = new File(dirPath);
		
		if(f.exists()) {
			f.delete();
			out.println(FTPCode.C250);
		} else {
			out.println(FTPCode.C550);
		}
	}
}
