package fil.sr1;

import java.io.IOException;

/**
 * Class représentant le thread lancé après une interruption du programme (ex : CTRL+C)
 */
class ShutdownHook extends Thread { 

    private FTPServer ftpServer;

    public ShutdownHook(FTPServer ftpServer){
        this.ftpServer = ftpServer;
    }
    
    public void run() { 
        System.out.println("Fermeture du serveur FTP ...");
        System.out.flush();
        try {
            this.ftpServer.close();
        } catch (IOException e) {
            System.out.println("Impossible de fermer le Serveur FTP");
            e.printStackTrace();
            System.out.flush();
        }
          
    } 
} 