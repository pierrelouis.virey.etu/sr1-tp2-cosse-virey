package fil.sr1;

import java.awt.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;

import fil.sr1.exception.WrongArgumentsException;
import fil.sr1.user.User;
import fil.sr1.user.UserManager;

/**
 * Permet de parser la commande et de lancer le serveur.
 */
public class Main {
	
	/**
	 * Récupère les users. L'idée et que si ce n'était pas un TP la méthode récupèrerait les id et mdp sur un fichier cryptée par exemple
	 */
	public static UserManager fetchUsers() {
		UserManager um = new UserManager();
		User user1 = new User("Theophile","azerty");
		User user2 = new User("PL","hAck3r");
		
		um.addUser(user1);
		um.addUser(user2);
		
		return um;
	}
	
	/**
	 * Permet de lancer le serveur avec les bon arguments
	 * @param args les arguments passées en ligne de commande
	 * @throws WrongArgumentsException 
	 */
	public static void launch(String[] args) throws WrongArgumentsException {
		
		FTPServer ftpServ;

		ShutdownHook shutdownHook;
		
		if(args.length == 1) {	//Un port
			int port = Integer.parseInt(args[0]);
			File dir = new File("./ftpdir");
			if(!dir.exists())
				dir.mkdir();
			
			ftpServ = new FTPServer(port,fetchUsers());
		} else if(args.length == 2) {	//Un port + un dossier.
			int port = Integer.parseInt(args[0]);
			String path = args[1];
			File dir = new File(path);
			
			if(dir.exists() && dir.isDirectory()) {		//On vérifie que le dossier existe bien.
				ftpServ = new FTPServer(port,fetchUsers(),path);
			} else {
				System.out.println("Le chemin "+path+" ne pointe pas vers un repertoire accessible");
				throw new WrongArgumentsException();
			}
		} else {
			throw new WrongArgumentsException();
		}
		
		shutdownHook = new ShutdownHook(ftpServ);
		Runtime.getRuntime().addShutdownHook(shutdownHook);
		
		try {
			ftpServ.start();		//Lancement du serveur
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
    public static void main(String[] args) throws IOException, InterruptedException { 	
    	try {
			launch(args);
		} catch (WrongArgumentsException e) {
			throw new RuntimeException(e);
		}
    }
}
