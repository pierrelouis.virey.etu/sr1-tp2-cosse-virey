Application ServeurFTP : TP2 de SR1  
Cosse Théophile  
Virey Pierre-Louis  
07/03/21  

# Introduction

Cette application permet de démarrer un serveur FTP à partir d'une ligne de commande.  
L'implémentation de ce programme utilise simplement la librairie standard JAVA et se compile avec l'outil Apache Maven.  

# Usage et installation

1. La compilation de ce programme est faite avec l'outil Apache Maven, pour l'obtenir : `apt install maven`
2. Cloner le repository : `git clone https://gitlab.univ-lille.fr/pierrelouis.virey.etu/sr1-tp2-cosse-virey.git`  
3. Déplacez-vous dans le répertoire : `cd sr1-tp2-cosse-virey`  
4. Créez un exécutable : `mvn package`  
5. Pour lancer le programme plusieurs options sont disponibles qui suivent la syntaxe suivante :  
`java -jar ServFTP-1.0.jar server_port [directory]`  
Le port du serveur à utiliser est obligatoire, le repertoire *directory* ne l'ai pas.  
Si le repertoire du serveur FTP n'est pas précisé, le programme va chercher un dossier ftpdir dans le dossier courant, s'il n'existe pas il sera crée.  
Si un repertoire est précisé il servira de racine au serveur FTP. Cependant s'il n'existe pas dans le système de fichiers de l'utilisateur il ne sera pas créé.  

Attention actuellement les seuls personnes pouvant se connecter au serveur sont les suivantes user : Theophile mdp : azerty et user : PL mdp : hAck3r  

# Architecture

L'architecture de ce programme est simple. Voici un diagramme de classe de l'application :

![](doc/dia_uml.png)

La classe main permet de créer le FTPServer avec les bons arguments ansi que la liste des users connus dans un user manager.  
Dès qu'un utilisateur se connecte, le serveur décrit par la classe FTPServer crée un Thread du type FTPClientThread qui s'occupe des interactions avec l'utilisateur.
La classe FTPCode contient les codes ainsi que les messages de description qui permettent de dialoguer avec le client.
La classe UserManager permet de gérer les utilisateurs du service FTP. Elle contient une hashmap d'User et permet de faire des "requetes" sur cette hashmap.
La classe ShutdownHook a pour but de fermer proprement les communications lors d'une fermeture du serveur.

### Méthode polymorphes :

- Le constructeur de la classe FTPServer est polymorphe. Il existe sous 2 signatures différentes : FTPServer(int port, UserManager userManager) et FTPServer(int port, UserManager userManager, String root).
La seconde version permet de préciser le repertoire racine du serveur qui servira pour les futurs échanges.

### Gestion des erreurs:

Nous avons plusieurs exceptions custom afin de signaler une erreur plus précisement :  

![](doc/diag_exep.png)

- ClientCloseException indique une erreur lors de la fermeture des sockets dans FTPClientThread.
- FTPClientThreadCreationException indique une erreur à la création du thread s'occupant des traitement client.
- WrongArgumentsException indique une erreur dans les arguments de la commande.

Dans cette applications les exceptions n'entrainent pas forcement l'arrêt du serveur car une mauvaise instruction d'un client pourrait provenir d'une défaillance réseau ou par exemple du fait que la copie locale des fichiers de l'utilisateur n'est pas à jour avec le serveur et qu'il souhaite faire une opération sur un fichier supprimé.  
 
 Voici une liste de la gestion des erreurs :
 
 **Main**
 - launch() throw l'exception WrongArgumentsException si il y a une erreur de syntaxe dans la commande.
 - launch() catch de l'IOException throw au lancement du serveur.
 - main() catch de l'exception WrongArgumentsException.  
 
 **FTPServer**
 - start() throw l'exception IOException lors de la création du ServerSocket.
 - start() catch de l'exception SocketException lorsque que le socket.accept est interrompue.
 
**FTPClientThread**
Lors de l'execution du code FTPClientThread un exception de type FTPClientThreadCreationException est crée signalement une erreur lors de la création du socket nécessaire à l'ouverture du canal de données.
Dans certain cas nous préfèrons envoyer au client un code d'erreur qui signale qu'il y a une erreur interne dans la création de socket ou les opérations sur fichiers.

 
 # Code Samples

### La fonction run() de FTPClientThread

La fonction run() de FTPClientThread permet d'appeler avec un nouveau thread l'execution du code ci-dessous.  
La fonction a pour but dans un premier temps d'envoyer une message d'accueil au client en ensuite de perpetuellement attendre toutes ses commandes pour les executer. 
```java
public void run() {
	out.println(FTPCode.C220);			// Message d'accueil
	String reponse;

	// On attend les commandes client.
	while(true) {
		reponse = in.readLine();
			if (reponse == null) {		// Interruption de la connexion au serveur.
				if (this.username != null)
					System.out.println("Connection de "+ this.username +" au serveur FTP interrompue.");
				break;
		} else {
			parseResponse(reponse);		// Parsage de le réponse pour pouvoir executer la commande.
		}
	}
}
```

### Le principe d'authentification.

Les fonctions suivantes permettent l'authentifaction d'un utilisateur qui tente de se connecter.  
Le refus de la connexion se fait seulement dans le do_PASS afin de ne pas faire savoir si un utilisateur existe ou non dans le système.  
```java
/**
 * Identification avec un username.
 */
private void do_USER(String username) {
	this.username = username;	// Identifiant du client
	out.println(FTPCode.C331);	// Dans tous les cas on "accepte la connexion" même si l'utilisateur est incconu.
}
	
/**
 * On attend le mot de passe pour finaliser la connexion.
 */
private void do_PASS(String password) {
	// Seulement ici on vérifie que l'user existe et le mdp correspondent.
	if(userManager.isUser(this.username) && userManager.getPassword(this.username).equals(password)) {
		out.println(acceptClient());			// Le client est reconnu on accepte sa connexion.
	} else {
		out.println(FTPCode.C530);			// Le client est inconnu fermeture de la connexion.
	}
}
	
/**
 * Crée un repertoire pour l'user client.
 * @return le code de validation de la commande
 */
private String acceptClient(){
	this.isAuthentified = true;		//La connexion est désormé authentifiée
	File dir = new File(this.homeDirectory+'/'+this.username);
	this.homeDirectory += '/'+this.username;
	... // Creation du repertoire client.
	
	return FTPCode.C230;
}
```
 
### La commande CWD et le système de fichier

Ce code sample illustre le fonctionnement du système de repertoire courant et de repertoire racine.  
L'implémentation de la commande CWD fait en sorte de "rooter" l'utilisateur en bloquant les "..", il ne peut pas quitter sa racine. Pour remonter d'un dossier il faut utiliser la commande CDUP.  

```java
/**
 * Permet de changer le repertoire de travail courant. 
 */
private void do_CWD(String directory) {
	String dirPath;
	
	if(directory.contains("..")) {				// Empeche l'utilisateur de remonter dans les fichiers du serveur.
		out.println(FTPCode.C501 + " Le serveur ne supporte pas les CWD avec ..");
	}
	
	if(directory.startsWith("/")) {				// cd est absolu par rapport au serveur.
		dirPath = homeDirectory + directory;
		File f = new File(dirPath);
		
		if(f.exists() && f.isDirectory()) {
			this.curDirectory = directory;		// Mise à jour du current directory
			out.println(FTPCode.C250);
		} else {
			out.println(FTPCode.C550);			// Le fichier n'est pas accessible.
		}
		
	} else {									// cd est relatif au repertoire courant.
		dirPath = homeDirectory + this.curDirectory + "/" + directory;
		File f = new File(dirPath);
		
		if(f.exists() && f.isDirectory()) {
			this.curDirectory =  this.curDirectory + "/" + directory;
			out.println(FTPCode.C250);
		} else {
			out.println(FTPCode.C550);			// Le fichier n'est pas accesible.
		}
	}
}
```

### Connexion en mode passif avec la commande PASV

Le serveur FTP accepte la commande PASV, permettant d'établir une connexion de type passive entre le client et le serveur. En mode passif c'est le serveur qui crée le socket du canal de données et demande au client de s'y connecter. Le code suivant correspond à la création du socket et à l'envoi de la réponse précisant l'adresse et le port à utiliser.

```java
	/**
	 * Creer les sockets permetant la communication passive et envoie au client les informations nécessaire pour sa connection.
	 */
	private void do_PASV() {
		// La convention est d'établire le port de donnée sur (port de controle - 1)
		int port = this.skt.getPort()-1;
		String formatedIP = IP.replace('.', ',');
		int port1 = port / 256;
		int port2 = port % 256;

		// Boolean permettant de savoir si fermer le ServerSocket et necessaire par la suite.
		this.isUsingPASV = true;

		String pasvInfos = FTPCode.C227 + " (" + formatedIP + "," + String.valueOf(port1) + "," + String.valueOf(port2) + ")";
		
		out.println(pasvInfos);		//Envoie des informations au client.

		try {
			this.servSkt = new ServerSocket(port);
			this.sktData = servSkt.accept();			
			//Création du socket permettant la connection.
			this.inData = new BufferedReader(new InputStreamReader(sktData.getInputStream()));
			this.outData = new PrintWriter(sktData.getOutputStream(), true);
		} catch (IOException e) {
			out.println(FTPCode.C425);
		}
	}
```

### Arrêt du serveur avec ShutdownHook

Suite à un arrêt du serveur (dû à une interruption CTRL-C ou à un crash par exemple) il est important de s'assurer que le serveur arrête proprement les différents sockets utilisés afin de libérer les ports pour d'autres applications. Lorsque la JVM s'arrête il est possible d'appeler un dernier thread grace à la classe ShutdownHook, on appelle ici un thread qui va s'occuper de fermer proprement le serverSocket gérant les connexions au serveur FTP.

```java
	// Main.java
	shutdownHook = new ShutdownHook(ftpServ);
	Runtime.getRuntime().addShutdownHook(shutdownHook);

	// ShutdownHook.java
    public void run() { 
        System.out.println("Fermeture du serveur FTP ...");
        System.out.flush();
        try {
            this.ftpServer.close();
        } catch (IOException e) {
            System.out.println("Impossible de fermer le Serveur FTP");
            e.printStackTrace();
            System.out.flush();
        }
          
    }
```




	


